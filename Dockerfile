FROM alpine:latest

RUN apk add --no-cache bash git openssh

WORKDIR /application

RUN mkdir /root/.ssh/ && chmod -R 700 /root/.ssh/ && touch /root/.ssh/known_hosts && ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts && ssh-keyscan gitlab.com >> /root/.ssh/known_hosts

COPY docker_remote_repo /root/.ssh/docker_remote_repo

COPY clone_repo_script.sh .

RUN chmod -R 600 /root/.ssh/docker_remote_repo && chmod +x clone_repo_script.sh

ENTRYPOINT ["./clone_repo_script.sh"]

CMD ["arg1", "arg2"]
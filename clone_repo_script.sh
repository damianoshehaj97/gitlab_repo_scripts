#!/bin/bash

eval $(ssh-agent -s)
ssh-add /root/.ssh/docker_remote_repo

if [ "$#" -ne 2 ]; then
   echo "Need exactly 2 arguments: <source-repo-url> <destination-repo-url>"
   exit 1
fi

SOURCE_REPO_URL="$1"
TARGET_REPO_URL="$2"

echo $SOURCE_REPO_URL
echo $TARGET_REPO_URL

TMP_DIR="tmp_clone_dir"
mkdir $TMP_DIR

git clone -v $SOURCE_REPO_URL $TMP_DIR
cd $TMP_DIR

git remote add gitlab $TARGET_REPO_URL

git push gitlab --all

cd ..
rm -rf $TMP_DIR

echo "Repository cloned from Bitbucket to Gitlab"
